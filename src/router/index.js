import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/entreprise',
      name: 'entreprise',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/entrepriseVue.vue')
    },
    {
      path: '/remerciements',
      name: 'remerciements',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/remerciementsVue.vue')
    },
    {
      path: '/presentation',
      name: 'presentation',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/presentationVue.vue')
    },{
      path: '/reflexions',
      name: 'reflexions',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/reflexionsVue.vue')
    },
    {
      path: '/temoignages',
      name: 'temoignages',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/temoignagesVue.vue')
    },
    {
      path: '/equipe',
      name: 'equipe',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/equipeVue.vue')
    },
    {
      path: '/etudes',
      name: 'etudes',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/etudesUX.vue')
    },
    {
      path: '/realisations',
      name: 'realisations',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/realisationVue.vue')
    },
    {
      path: '/bilan',
      name: 'bilan',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/bilans.vue')
    },
    {
      path: '/404',
      component: () => import(/* webpackChunkName: "about" */ '../views/page404.vue')
    },
    {
      path: "/:catchAll(.*)",
      redirect: '/404'
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    // return desired position
    return { top: 0 }
  }
})

export default router

